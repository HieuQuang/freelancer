from django.urls import path

from django.conf.urls.static import static
from django.conf import settings

from .views import (
    ArticleListView,

#     ArticleListView1,
    ArticleUpdateView,
    ArticleDetailView,
    ArticleDeleteView,
    ArticleCreateView,  # new
    ArticleformView,
    BlogListView,
    BlogUpdateView,
    BlogDetailView,
    BlogCreateView,
    BlogDeleteView,    
)
urlpatterns = [
    path('<int:pk>/edit/',
         ArticleUpdateView.as_view(), name='article_edit'),
    path('<int:pk>/',
         ArticleDetailView.as_view(), name='article_detail'),
    path('<int:pk>/delete/',
         ArticleDeleteView.as_view(), name='article_delete'),

    path('<int:pk>/form/',
         ArticleformView.as_view(), name='article_form'),

    path('new/', ArticleCreateView.as_view(), name='article_new'),
    
    path('list/', ArticleListView.as_view(), name='article_list'),


#     path('<int:pk>/saveCV/',
#      ArticleListView1.as_view(), name='article_hienthi'),

    path('post/<int:pk>/delete/', BlogDeleteView.as_view(), name='post_delete'),
    path('post/<int:pk>/edit/', BlogUpdateView.as_view(), name='post_edit'),
    path('post/new/', BlogCreateView.as_view(), name='post_new'),
    path('post/<int:pk>/', BlogDetailView.as_view(), name='post_detail'),
    path('', BlogListView.as_view(), name='home'),


   
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
